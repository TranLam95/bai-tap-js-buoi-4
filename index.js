
// Start Bài 1
function ketQua(){
    var a = document.getElementById("txt-number-1").value*1;
    var b = document.getElementById("txt-number-2").value*1;
    var c = document.getElementById("txt-number-3").value*1;
    if (a>b && b>c){
        document.getElementById("result").innerHTML = `${a} ${b} ${c}`;
    } else if (a>c && c>b){
        document.getElementById("result").innerHTML = `${a} ${c} ${b}`;
    } else if (b>a && a>c){
        document.getElementById("result").innerHTML = `${b} ${a} ${c}`;
    }else if (b>c && c>a){
        document.getElementById("result").innerHTML = `${b} ${c} ${a}`;
    }else if (c>b && b>a){
        document.getElementById("result").innerHTML = `${c} ${b} ${a}`;
    } else if (c>a && a>b){
        document.getElementById("result").innerHTML = `${c} ${a} ${b}`;
    }
}
// End Bài 1
// Start Bài 2
function sapXep(){
    var thanhVien = document.getElementById("txt-thanh-vien").value;
    document.getElementById("result1").innerHTML = `<p>Xin chào ${thanhVien}!</p>`;
}
// End Bài 2
// Start Bài 3
function soLuong(){
    var soMot = document.getElementById("txt-so-1").value*1;
    var soHai = document.getElementById("txt-so-2").value*1;
    var soBa = document.getElementById("txt-so-3").value*1;
    var soChan = 0;
    var soLe = 0;
    if (soMot%2==0){
        soChan++;
    }else {
        soLe++
    }
    if (soHai%2==0){
        soChan++;
    }else {
        soLe++
    }
    if (soBa%2==0){
        soChan++;
    }else {
        soLe++
    }
    document.getElementById("result2").innerHTML= `<p> Có ${soChan} số chẵn và có ${soLe} số lẻ`;
}
// End Bài 3
// Start Bài 4
function dapAn(){
    var canhMot = document.getElementById("txt-canh-1").value*1;
    var canhHai = document.getElementById("txt-canh-2").value*1;
    var canhBa = document.getElementById("txt-canh-3").value*1;
    if (canhMot==canhHai || canhMot==canhBa || canhHai==canhBa){
        document.getElementById("result3").innerHTML=`Đây là tam giác cân`;
    }else if (canhMot==canhHai && canhMot==canhBa){
        document.getElementById("result3").innerHTML=`Đây là tam giác đều`;
    }else if (Math.pow(canhMot,2) + Math.pow(canhHai,2)==Math.pow(canhBa,2) || Math.pow(canhMot,2) + Math.pow(canhBa,2) == Math.pow(canhHai,2) || Math.pow(canhBa,2) + Math.pow(canhHai,2)== Math.pow(canhMot,2)){
        document.getElementById("result3").innerHTML=`Đây là tam giác vuông`;
    }else{
        document.getElementById("result3").innerHTML=`Đây là tam giác khác`;
    }
}
// End Bài 4